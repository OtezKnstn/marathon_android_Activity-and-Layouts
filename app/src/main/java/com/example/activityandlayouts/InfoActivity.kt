package com.example.activityandlayouts

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val textView: TextView = findViewById(R.id.textView)
        val textSurname: String? = intent.extras?.getString("textSurname")
        val textName: String? = intent.extras?.getString("textName")
        val textPatronymic: String? = intent.extras?.getString("textPatronymic")
        val textAge: String? = intent.extras?.getString("textAge")
        val textHobby: String? = intent.extras?.getString("textHobby")

        val past_life = when(textAge?.toInt()?.div(20)) {
            0 -> getString(R.string.past_life1)
            1 -> getString(R.string.past_life2)
            2 -> getString(R.string.past_life3)
            3 -> getString(R.string.past_life4)
            else -> getString(R.string.past_life5)
        }

        """
            -Представься.
            - $textSurname $textName $textPatronymic
            - А твоё хобби?
            - Ну я люблю $textHobby.
            - Ооо, я вижу в прошлой жизни ты был $past_life и умер в $textAge, но вижу, что ты пережил его.
            - Но как же он умер?
            - Меньше знаешь - крепче спишь, друг мой.
        """.trimIndent().also { textView.text = it }
    }
}
