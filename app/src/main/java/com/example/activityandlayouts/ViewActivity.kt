package com.example.activityandlayouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_view.*

class ViewActivity : AppCompatActivity() {

    lateinit var viewModel: TestViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        viewModel = ViewModelProvider(this).get(TestViewModel::class.java)

        viewModel.currentNumber.observe( this, Observer {
            numOfClicks.text = it.toString()
        })
        incrementText()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return true

    }

    private fun incrementText() {
        btn_clicker.setOnClickListener {
            viewModel.currentNumber.value = ++viewModel.number
        }
    }
}
