package com.example.activityandlayouts

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TestViewModel: ViewModel() {

    var number: Int = 0

    val currentNumber: MutableLiveData<Int> by lazy{
        MutableLiveData<Int>()
    }
}