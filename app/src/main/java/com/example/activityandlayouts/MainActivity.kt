package com.example.activityandlayouts

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_splash.*

class MainActivity : AppCompatActivity() {

    private lateinit var button : Button
    private lateinit var editTextSurname: EditText
    private lateinit var editTextName: EditText
    private lateinit var editTextPatronymic: EditText
    private lateinit var editTextAge: EditText
    private lateinit var editTextHobby: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.btnOfTruth)
        editTextSurname = findViewById(R.id.editTextSurname)
        editTextName = findViewById(R.id.editTextName)
        editTextPatronymic = findViewById(R.id.editTextPatronymic)
        editTextAge = findViewById(R.id.editTextAge)
        editTextHobby = findViewById(R.id.editTextHobby)

        loadData()

        button.setOnClickListener {

            Intent(this, InfoActivity::class.java).also {
                it.putExtra("textSurname", editTextSurname.text.toString())
                it.putExtra("textName", editTextName.text.toString())
                it.putExtra("textPatronymic", editTextPatronymic.text.toString())
                it.putExtra("textAge", editTextAge.text.toString())
                it.putExtra("textHobby", editTextHobby.text.toString())
                startActivity(it)
                finish()
                }
        }
    }


    override fun onCreateOptionsMenu (menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){

            R.id.btn_save ->saveData()

            R.id.btn_bgr ->change_bckgr()

            R.id.btn_game ->{
                Intent(this, ViewActivity::class.java).also{
                    startActivity(it)
                    finish()
                }

            }
        }

        return true
    }
    private fun saveData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putString("textSurname", editTextSurname.text.toString())
            putString("textName", editTextName.text.toString())
            putString("textPatronymic", editTextPatronymic.text.toString())
            putString("textAge", editTextAge.text.toString())
            putString("textHobby", editTextHobby.text.toString())
        }.apply()
        Toast.makeText(this, getString(R.string.toast_save), Toast.LENGTH_SHORT).show()
    }

    private fun loadData() {

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val savedSurname = sharedPreferences.getString("textSurname", null)
        val savedName = sharedPreferences.getString("textName", null)
        val savedPatronymic = sharedPreferences.getString("textPatronymic", null)
        val savedAge = sharedPreferences.getString("textAge", null)
        val savedHobby = sharedPreferences.getString("textHobby", null)

        editTextSurname.setText(savedSurname)
        editTextName.setText(savedName)
        editTextPatronymic.setText(savedPatronymic)
        editTextAge.setText(savedAge)
        editTextHobby.setText(savedHobby)

    }

    private fun change_bckgr() {
        when ((1..6).random()) {
            1 -> R.drawable.perviy
            2 -> R.drawable.vtoroy
            3 -> R.drawable.tretiy
            4 -> R.drawable.chetvertiy
            5 -> R.drawable.pyatiy
            else -> R.drawable.shestoy
        }.also{
            image_bgr.setBackgroundResource(it)
        }
    }

}

